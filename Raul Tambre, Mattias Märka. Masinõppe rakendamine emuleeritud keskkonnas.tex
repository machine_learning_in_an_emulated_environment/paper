% !TeX program = xelatex
\documentclass{vilgym}

\usepackage{pythonhighlight}
\usepackage{tikz}
\usetikzlibrary{arrows, positioning}

% General info
\title{Masinõppe rakendamine emuleeritud keskkonnas}
\authors{Raul Tambre, Mattias Märka, III MF}
\instructor{õp Marika Anissimov}
\date{2019}

\babelhyphenation[estonian]{ma-sin-õp-pe-prog-ramm}

\begin{document}
	\maketitle
	\spacing{1.04} % Decrease spacing, so the TOC fits neatly on one page.
	\tableofcontents
	\spacing{1.4}

	\unsection{Definitsioonid}
	\begin{description}
		\let\originalitem\item
		\renewcommand*{\item}[1][]{\originalitem[#1]\label{def:#1}}

		\item[emulaator] ingl.k. \textit{emulator} on riistvara või tarkvara, mis matkib mõnda teist süsteemi.
		\item[võrgupesa] ingl.k. \textit{network socket} on operatsioonisüsteemi üksus, mis võimaldab programmidel üle võrgu teiste võrgupesadega ühenduda ja andmeid vahetada. Kasutatakse kõigis üle võrgu suhtlevates programmides.
		\item[lõim] ingl.k. \textit{thread} on protsessi alamüksus, mis võimaldab programmil omada mitut paralleelset täitmisvoogu ja mis jagab süsteemi ressurse teiste lõimudega.
		\item[ROM] ingl.k. \textit{Read-only Memory} ehk ainult loetav mälu on mäluseade, mille andmed säilivad püsivalt ja mille väärtusi pole võimalik muuta.
		\item[MMIO] ingl.k. \textit{Memory-mapped Input/Output} ehk mälukaardistatud sisend/väljund on mälupiirkond või asukoht, mis võimaldab manipuleerida ja/või lugeda andmeid mõnesuguselt riistvara komponendilt.
		\item[implementeerima] ehk evitama, seadmestama. Midagi juurde lisama või täide viima.
		\item[klass] on keelekonstruktsioon, mida saab kasutada sarnaste omadustega objektide loomiseks.
	\end{description}

	\newcommand*{\seedefinition}[1]{(\hyperref[def:#1]{vt~definitsiooni})}

	\unsection{Sissejuhatus}
	Tänapäeval on tarkvaraarendusprojektid kogukad ja keerulised. Ühe projekti kallal võib korraga töötada sadu inimesi ning projekt võib olla nii suur, et mitte keegi ei suuda kõige toimuvaga kursis püsida. Sestap peab projekte olema lihtne edasi arendada ja vajadusel parandada.

	Riistvara kiire arenguga on esiplaanile kerkinud kaks väga erinevat arvutiteaduse haru: emulatsioon ja masinõpe. Emulatsioon on vajalik vana tarkvara säilitamiseks ja jätkuvaks kasutamiseks. Masinõpet kasutatakse väga erinevateks otstarveteks, alustades rämpsposti tuvastamisest, lõpetades majandusnäitajate ennustamisega.

	Praktilise uurimistöö eesmärk on luua emuleeritud keskkond, milles on võimalik masinõpet kasutada, ja sinna juurde käiv näidismasinõppeprogramm. Nii emulatsioonis kui ka masinõppes kasutatakse testobjektidena mänge. Seetõttu sobib need teemad omavahel ühendada.

	Hüpoteesiks on, et omades vaid teadmisi programmeerimist, on võimalik läbi viia taoline tarkvaraarendusprojekt.

	Töös kasutame kvalitatiivset meetodit, analüüsides teiste poolt koostatud dokumentatsiooni. Praktilise töö käigus testime ja analüüsime, kas ja kui hästi erinevad projekti osad töötavad.

	Töö jaotame ära nii, et Raul loob emulaatori ja Mattias masinõppeprogrammi ning kõik, mis selle juurde käib.

	\section{Emulaator}
	\subsection{Emuleeritava süsteemi valik}
	Peamiseks eesmärgiks süsteemi valikul oli, et emulaatorit valitavale süsteemile oleks sobivalt lihtne implementeerida vastavalt ajapiirangutele, kuid siiski piisavalt keerukas, et lõplik visuaalne tulemus oleks huvitav.

	Seetõttu valisime emuleeritavaks süsteemiks 1989. aastast pärit GameBoy pihukonsooli.
	Konsooli protsessor on tänapäevaste arvutitega võrreldes väga aeglane ja sellest tulenevalt lihtsasti emuleeritav interpreteeriva emulaatoriga. Süsteemil on vähe lisakomponente, millest mõningaid ei ole vajalik emuleerida, et meie poolt valitud mängud oleksid mängitavad ilma vigadeta -- nt ei ole vajalik häälekiibi ja mõningate keerulisemate renderdamisfunktsioonide emuleerimine.
	GameBoy süsteemi on juba hoolikalt uuritud, ning seetõttu on olemas detailne dokumentatsioon, mis teeb emulaatori loomise kordades lihtsamaks. Eksisteerib juba palju avatud lähtega täpseid emulaatoreid, mille abil saab kontrollida tulemuse korrektsust. Süsteemi ümber on kujunenud väike kogukond, kes senini loovad süsteemile mänge ja kelle poole on võimalik pöörduda küsimuste korral konsooli toimimise kohta.

	\subsection{Oma emulaatori vajadus}
	Võib tekkida küsimus, miks ei sobi mõni avatud lähtega olemasolev emulaator ja on vaja luua enda oma.
    
	Masinõppeprogrammi kasutamiseks emulaatoriga on vaja rakendusliidest, mille kaudu masinõppeprogramm saaks emulaatoris toimuvat manipuleerida. Meie teada aga pole ühelgi olemasoleval avatud lähtega emulaatoril taolist sobivat rakendusliidest.
	Lisaks sellele annab enda emulaatori loomine rohkem vabadust lähtekoodi jagamisel ja kasutamisel, sest meil on käed vabad litsentsi valimisel. Samuti aitab emulaatori loomine konsooli ja selle mängude hingeelust paremini aru saada.

	\subsection{Implementatsioon}
	\subsubsection{Tüüp}
	Instruktsioonide täitmise järgi saab emulaatoreid jagada kaheks:
	\begin{description}
		\item[interpreteerivad] emulaatorid, mis emuleerivad iga instruktsiooni käitumist eraldi. Lihtsad \linebreak implementeerida, kuid aeglased.
		\item[rekompileerivad] emulaatorid, mis teisendavad instruktsioonid emuleerivale masinale arusaadavateks instruktsioonikogumiteks. Palju kiiremad, kuid raskemad implementeerida võrreldes interpreteerivatega.
	\end{description}

	Kuna GameBoy on väga aeglane võrreldes tänapäevaste arvutitega, siis on kõik sellel jooksvad programmid kuni mitu korda kiiremini reaalajast emuleeritavad lihtsa interpreteeriva emulaatoriga. Antud süsteemil on võimalik, et esineb ennastmuutvat koodi, mis teeks rekompilaatori loomise veelgi raskemaks ning mille esinemise korral on tõenäoline, et kahaneks tunduvalt rekompilaatorist saadav kasu.

	Otsustasime valida seega interpreteeriva emulaatori, kuna rekompilaatori kirjutamine oleks olnud liiga mahuks ja ei oleks meie uurimistöö jaoks nähtavat kasu toonud.

	\subsubsection{Programmeerimiskeele valik}
	Otsustasime emulaatori jaoks valida mõne staatilise tüübisüsteemiga kompileeritava programmeerimiskeele. See kindlustab, et interpreteeritav emulaator ei jää liiga aeglaseks ning aitab vältida vigu. Eelneva kogemuse tõttu valisime C++'i, mis on ISO poolt standardiseeritud. Kasutasime kõige uuemat 2017. aastal avaldatud keele varianti ISO/IEC~14882:2017.\footnote{ISO/IEC 14882:2017. \url{https://www.iso.org/standard/68564.html} (12.04.2018)}

	\subsubsection{Rakendusliides}
	Rakendusliides töötab üle võrgupesa \seedefinition{võrgupesa}, mille emulaator avab käivitudes kohalikul \textit{loopback} liidesel. Järgnevalt saab mõni teine programm (antud uurimistöös masinõppeprogramm) võrgupesaga ühenduda. Seejärel jätkub suhtlus programmide vahel võrgupesa kaudu binaarprotokolliga, mis on emulaatori dokumentatsioonis spetsifitseeritud.

	Emulaatoril on kolm erinevat lõimu \seedefinition{lõim}:
	\begin{itemize}
		\item liidese lõim -- kindlustab emulaatori graafilise liidese toimimise;
		\item emulatsiooni lõim -- emuleerib ja toetab rakendusliidese tööd;
		\item rakendusliidese lõim -- tegeleb rakendusliidesele saabuvate sõnumitega.
	\end{itemize}

	Teatud käskude puhul sünkroniseerib rakendusliidese lõim oma tegevust emulatsiooni lõimuga, nt emuleerimise ajutiseks peatamiseks või teatud sündmustest (nt kaadri valmimine) rakendusliidese kaudu teavitamiseks.

	\subsection{Ettetulnud probleemid}
	\subsubsection{ROM'i kirjutamine}
	Emulaatori katsetamise ajal ilmnesid kahes testitavas mängus, \enquote{Dr. Mario} ja \enquote{Tetris}, väiksemad vead. \enquote{Dr. Mario} puhul, mis sarnaneb \enquote{Tetrisega}, kukkusid tükid, mida mängija peab teatud viisil mänguväljale asetama, mõningatel juhtudel läbi teistest tükkidest. Lisaks osad paigutatud plokkide viirud eemaldusid mängust ebakorrapäraselt (plokkide viirud peavad eemalduma ainult siis kui on 4 samavärvilist plokki reas).\footnote{\enquote{Dr. Mario} katkine põrkumine. \url{https://www.youtube.com/watch?v=ORxFeR3cU6M} (19.10.2018)} \enquote{Tetrises} aga kasvas skoor eksponentsiaalselt, mitte lineaarselt nagu tavaliselt, mis tähendas, et mängija võis saavutada maksimaalse võimaliku punktisumma vaid mõne minuti jooksul.

	Peale pikka mängu \enquote{Dr. Mario} silumist ja koodi jooksuteekonna võrdlemist täpse emulaatoriga selgus, et mäluaadressis $2000_{16}$ asuv väärtus oli muutunud, mis põhjustas instruktsiooni erinevuse ja kõrvalekaldumise mängukoodi jooksmises. Täpselt sama olukord ilmnes \enquote{Tetrises}.

	Mäluaadressi väärtuse erinemine tulenes sellest, et mäng prooviski kirjutada tollele aadressile teise väärtuse (ka korrektses emulaatoris). Korrektne on hoopis aga ignoreerida kirjutust ja mäluväärtust mitte muuta, sest aadress $2000_{16}$ asub ROM piirkonnas \seedefinition{ROM}.

	Mängu põhjus kirjutamaks mäluaadressi, mille väärtust ei ole võimalik muuta on MMIO \seedefinition{MMIO} asumine tolles mälupiirkonnas.
	Kuna mälumaht on väga limiteeritud, siis on võimalik mängukassetil teatud mälukirjutuste puhul ROM piirkonnas vahetada välja protsessorile mängukassetilt edastatavaid andmeid. See võimaldab mängukassetil hoida rohkem mängule vajalikku koodi ja andmeid, ning seeläbi luua keerukamaid mänge.
	Siiski nii \enquote{Dr. Mariot} kui ka \enquote{Tetrist} ei toodetud mängukassettidel, millel oleks selline funktsionaalsus. Võib eeldada, et too mälukirjutus lisati mängukoodi, et mäng oleks töövõimeline juhul, kui kunagi oleks hakatud tootma mängu taoliste lisafunktsioonidega mängukassettidel. Vastasel juhul on võimalik, et oleks olnud vajalik mängukoodi muuta selle kirjutuse lisamiseks, ning toota juurde uue mänguversiooniga kiipe, mis ei oleks olnud kuigi odav.

	\subsubsection{Võimalik viga Windowsis}
	Rakendusliidese lõim veedab enamuse ajast oodates ehk magades. Lõim äratatakse üles alles siis, kui võrgupesa on kinni pandud või võrgupessa on saabunud andmeid, mida on võimalik lugeda. Üles ärgates teeb lõim seejärel kindlaks, kas võrgupesa sulgus, kui ei, siis eeldab, et andmed saabusid (\hyperref[fig:töökäik]{vt joonis 1}). Lõim reageerib mõlemale juhule sobivalt.

	\begin{figure}[h]
		\begin{tikzpicture}[node distance=1cm and 1.3cm]
			\draw[fill=black]
			node[label={algus}](algus){} circle(4pt)
			node[draw,right=of algus](maga){magamine}
			node[draw,right=of maga](kumb){kumb juhtus?}
			node[draw,right=of kumb](andmed){andmete lugemine ja käsitlemine}
			node[draw,right=of kumb,above=of andmed](sulg){sulgumise käsitlemine};

			\draw[-latex,rounded corners]
			(algus) edge (maga)
			(maga) edge (kumb)
			(kumb) edge (andmed)
			(andmed) -- +(0,-1) -| (maga);

			\draw[-latex,rounded corners] (kumb) |- (sulg);
		\end{tikzpicture}
		\caption{Rakendusliidese lõimu töökäik}\label{fig:töökäik}
	\end{figure}

	Masinõppeprogrammi katsetamise ajal hakkas aga emulaator sulguma, sest andmete lugemine rakendusliidese lõimus ebaõnnestus. Peale mõningat silumist selgus, et nurjumine oli tingitud loetavate andmete puudumisest. Silumine näitas, et võrgupesa ei olnud siiski sulgunud. Ehk siis lõim äratati üles, kuigi tegelikult ei olnud ülesäratamiseks vajalikku tingimust rahuldatud~-- kas andmeid on saabunud või pesa sulgunud.
	Probleem oli järjepidev ja lihtsasti reprodutseeritav mitmel arvutil.

	Silumisel avastasime, et funktsioon, millega tegime kindlaks, mis täpselt põhjustas äratuse, tagastas, et midagi ei olnud juhtunud. Seetõttu probleemi parandamiseks me muutsime koodi nii, et see läheks jälle magama juhul, kui midagi ei olnud juhtunud.

	Vastavas koodis ei avastanud me ühtegi viga. Vastavate funktsioonide dokumentatsioonist ei leidnud me midagi, mis oleks maininud erisusi meie lähenemise juures. Kood oli kooskõlas dokumentatsioonis leiduvate näidetega. Kuna eeltoodud funktsioon tagastas, et midagi ei olnud juhtunud peale ärkamist, siis me eeldame, et Windowsis on viga, mis põhjustab taoliste lõimude ülesäratamist ka juhul, kui selleks vajalikud tingimused ei ole rahuldatud.

	\section{Sidekomponent}
	\subsection{Otstarve}
	Kuna emulaatori ja masinõppeprogrammi liidesed on niivõrd erinevad, on vaja lisa abstraktsiooni, sidekomponenti, mis teeb nendevahelise suhtlemise mugavamaks.

	Ühelt poolt ühendub sidekomponent emulaatoriga, teiselt poolt masinõppeprogrammiga. Selleks, et ühendada masinõppeprogrammi külge mõni teine emulaator või emulaatori külge mõni sootuks teine programm, on vaja muuta ainult sidekomponenti, mitte ülejäänud koodi.

	Niisugune abstraktsioon tagab projekti modulaarsuse (\hyperref[fig:modularity]{vt joonis 2}).

	\begin{figure}[h]
		\begin{tikzpicture}[node distance=1cm and 2.75cm]
			\draw[fill=black]
			node[draw](emu){emulaator}
			node[draw,right=of emu](side){sidekomponent}
			node[draw,right=of side](user){kasutaja};

			\draw[-latex]
			([yshift=0.10cm]emu.east) to node[midway, above=-0.3cm, label=above:{1. protokoll}]{} ([yshift=0.10cm]side.west);

			\draw[-latex]
			([yshift=-0.10cm]side.west) to ([yshift=-0.10cm]emu.east);

			\draw[-latex]
			([yshift=0.10cm]side.east) to node[midway, above=-0.3cm, label=above:{2. protokoll}]{} ([yshift=0.10cm]user.west);

			\draw[-latex]
			([yshift=-0.10cm]user.west) to ([yshift=-0.10cm]side.east);
		\end{tikzpicture}
		\caption{Emulaatori, sidekomponendi ja kasutaja vaheline suhe}\label{fig:modularity}
	\end{figure}

	\subsection{Implementatsioon}
	Kuna masinõppeprogramm saab implementeeritud Pythonis, siis selleks, et sidekomponent \linebreak saaks sellega ilma vahelülideta suhelda, implementeerime ka selle Pythonis.

	Emulaatoriga saab suhelda rakendusliidese vahendusel.

	Prototüüpimise käigus selgus, et kõige mugavam on sidekomponent teha selline, et see matkib järgi olukorda, kus emulaator on samamoodi Pythonis implementeeritud. Defineerime emulaatori klassina, mille meetodid kutsuvad emulaatoris esile erinevaid muutusi \seeappendix{1}.

	Niisugune ülesehitus võimaldab ka eksimused enne emulaatorini jõudmist kinni püüda. Selle asemel, et eksimus tõstatatakse emulaatoris, tõstatatakse see sidekomponendis, mis teeb needsamad vead masinõppeprogrammile kergemini nähtavaks. Kui peaks minisugune viga esinema, siis sidekomponent peatab masinõppeprogrammi ja vajadusel emulaatori.

	Kuna sidekomponent on implementeeritud klassina on lihtne seda masinõppeprogrammi jaoks kohandada \seeappendix{2}.

	\section{Masinõppeprogramm}
	\subsection{Lähenemise valik}
	Masinõpe on praktilisse kasutusse jõudnud alles hiljuti, kuigi sellealast teooriat on arendatud juba mitukümmend aastat. Paraku olid arvutite võimsused pikka aega lihtsalt liiga madalad.

	Üldiselt võib masinõpet kirjeldada kui programmide loomise viisi, mis ei põhine mitte inimese läbinägelikkusel, vaid suurtel andmekogumitel. Masinõppe programmid jagatakse tüübi ja meetodi alusel.

	Tüübi alusel jagatakse:\footnote{Tüüpide ja meetodite jagamise joonis. \url{https://scikit-learn.org/stable/tutorial/machine_learning_map/index.html} (05.01.2019)}
	\begin{description}
		\item[juhendamisega õppe] puhul on teada sisend- ja väljundandmed ning otsitakse funktsiooni, mis nendevahelist seost kirjeldaks.
		\item[juhendamata õppe] korral püütakse vastavate väljundandmeteta sisendandmete hulgas leida uusi struktuure.
		\item[vähese juhendamisga õpe] on midagi juhendamisega ja juhendamata õppe vahepealset.
		\item[abistatud õppes] tegutseb programm mingisuguses keskkonnas ja üritab käituda nii, et maksimiseerida keskkonnast tulevat kasusignaali.
	\end{description}

	Mõned meetodid on:\footnotemark[\value{footnote}]
	\begin{description}
		\item[regressioonanalüüs] on ilmselt kõige lihtsam, andmehulga jaoks otsitakse sobivat matemaatilist funktsiooni, näiteks lineaar- või ruutfunktsiooni.
		\item[tugivektor-masinad] on samuti matemaatilise taustaga masinõppemeetod, mida kasutatakse \linebreak andmete alahulkades jagamiseks.
		\item[geneetiline programmeerimine] on evolutsiooni põhimõtetel töötav meetod, mille puhul otsitakse sobivat funktsiooni teatavate eelseatud vooruste alusel.
		\item[tehisnärvivõrgud] on bioloogilistest närvivõrkudest inspireeritud meetod, mille korral funktsiooni modelleeritakse närvivõrguna.
	\end{description}

	Kuna meie töö puhul on andmete vahelised seosed üpris keerulised, valime tehisnärvivõrgu meetodi. Andmete käsitsi märgistamine võtaks väga kaua aega seega sobib kõige paremini abistatud õpe. Sarnased uurimistööd on näidanud, et niisugune lähenemine võib osutuda päris edukaks.\footnote{Playing Atari with Deep Reinforcement Learning. University of Toronto. \url{https://www.cs.toronto.edu/~vmnih/docs/dqn.pdf} (05.01.2019)}

	\subsection{Implementatsioon}
	Kõige parem keel masinõppe jaoks on tänapäeval ilmselt Python. Pythoni masinõppeteekide valik on lai ning need on hästi dokumenteeritud. Sellepärast valimegi implementeerimise keeleks Pythoni, täpsemalt Python 3.6, sest see on hetkeseisuga kõige uuem versioon, mis toetab kõiki vajalike teeke.\footnote{Python 3.6.0. Python Software Foundation. \url{https://www.python.org/downloads/release/python-360/} (02.01.2019)}

	Teegiks valime Tensorflow.\footnote{Tensorflow. \url{https://www.tensorflow.org/} (26.11.2018)} Tensorflow on Google'i poolt arendatav masinõppeteek, mis koosneb paljudest erinevatest moodulitest, mis aitavad kasutajal võimalikult vähese aja- ja ressursikuluga valmistada töötav algoritm. Siinjuures tasub ära märkida, et Tensorflow ise pole Pythonis kirjutatud, vaid sellel on olemas Pythoni liides. Tensorflow jookseb Pythoni programmist eraldi ja on seeläbi kiirem. See on optimeeritud töötama uuema riistvara peal.

	Esialgu teeb algoritm suvalisi käike.\footnote{Masinõppeprogramm teeb suvalisi käike. \url{https://youtu.be/lhVbyhlWqfg} (12.01.2019)} Õppimisel eelistab algoritm neid käike, mille puhul on kasu kõige suurem, see tähendab niisugusid käike, mille tõttu skoor tõuseb. Õppimisel võtab algoritm arvesse praeguse ja paar eelmist kaadrit ning eelmise nupuvajutuse, kusjuures mitte millegi tegemine on ka võimalus. Algortim loob seosed kaadrites oleva info, nupuvajutuste ja tulemuse vahel. Edaspidi üritab algoritm teha ainult niisuguseid käike, mis tõotavad skoori tõsta. Seda protsessi korratakse mitu korda, mille käigus algoritm õpib paremini mängima.\footnote{Masinõppeprogramm mängib peale mõnetunnist treenimist. \url{https://youtu.be/0clKaacV9r4} (13.01.2019)}
	
	\begin{figure}[h]
		\includegraphics[width=9cm]{screen_comparison}
		\caption{Ekraanitõmmis (vasakul) ja masinõppealgoritmi sisend (paremal)}\label{fig:ekraan}
	\end{figure}

	Kui aga õppimist teha liiga kaua võib algoritm hoopis halvemaks jääda. On ka võimalus, et algoritm jääb mingisugusele kindlale tasemele lukku, sest õppimisandmed on liiga ühekülgsed.
	
	\subsection{Tulemus}
	Masinõppeprogramm töötas vahelduva eduga. Mida rohkem aega ja arvutiresursse treenimise peale kasutasime, seda parem oli tulemus. Vähesel määral sõltus tulemus ka juhusest, sellest kui hea andmestik moodustus suvalistest liigutustest.
	
	Paraku jäi arvutiresurssidest puudu. Treenimisprogramm jooksis paar aastat vana videokaardi peal kokku umbes 24 tundi. Sellest jäi aga väheks. Palju parema tulemuse oleks saanud, kui kasutada võimsamat arvutit, näiteks rentida selle jaoks mõeldud server.
	
	Treeninud algoritmi sooritus on piisavalt hea, et kinnitada hüpotees, aga jääb kõvasti alla inimmängijale. Muidugi on täiesti võimalik, et rohkema treenimisega saab algoritm paremaks kui inimene.
	
	\unsection{Kokkuvõte}
	Uurimistöö eesmärk oli luua emuleeritud keskkond, milles on võimalik masinõpet kasutada ja luua sinna juurde käiv näidismasinõppeprogramm. Meie uurimistöö tulemusena leidis hüpotees kinnitust. Muidugi ei saa öelda, kas seda oleks saanud teha ka paremini.

	Selgus, et tarkvaraarenduse jaoks, vähemalt valitud teemade jaoks, on vajalik informatsioon kergesti kättesaadav. Seda informatsiooni on võimalik ka amatööril ära kasutada ja selle põhjal midagi luua. Väga palju saab ära kasutada sarnastes uurimistöödes avastatut ja sarnastes tarkvaraarendusprojektides loodut.

	Muidugi ei paku niisugune lähenemine laialdasi teadmisi valitud teemal, aga piisavaid, et teoo\-riat praktikas ära kasutada.

	Projekti esimese osaga, emulaatoriga, võib päris rahule jääda. Emulaator täidab oma ülesande rahuldavalt ja seda on võimalik edasi arendada.

	Niisamuti võib rahule jääda projekti teise osaga, sidekomponendiga, mis töötab nagu vaja.

	Projekti kolmas osa, masinõppeprogramm on rahuldav. See töötab vahelduva eduga, piisavalt, et hüpotees kinnitada. Selle osa juures oleks eelnev teooria tundmine kindlasti suureks abiks olnud.

	Projekti erinevad osad töötavad omavahel kenasti, aga on piisavalt autonoomsed, et neid saaks kasutada ka eraldi mõnes teises projektis.

	See uurimistöö annab läbilõike tarkvaraarenduse maailmast valitud teemade seisukohalt. Siinjuures tasub ära märkida, et see maailm on koguaeg muutuv ja see, mis kehtib preagu, ei pruugi kehtida tulevikus.

	Töö käigus õppisime nii konkreetsete valdkondade kohta kui ka tarkvaraarenduse kohta üldiselt. Lisaks õppisime, kuidas niisuguseks projektiks ette valmistuda.

	Täname kõiki töö valmimisele kaasa aidanud inimesi, iseäranis juhendajat Marika Anissimovi.

	\unsection{Summary}
	The aim of the research paper was to create an emulated environment, in which machine learning can be used, and an example machine learning program to go along with it. Our hypothesis was confirmed as a result of our research. Of course, it is impossible to say if it could have been done better.

	It became apparent that for software development, at least for the chosen subjects, all the necessary information is easily findable. It is possible for even an amateur to use such information and create something based on it. A lot can be reused from similar research and software development projects.

	The given approach does not offer in depth knowledge of the subject, however it is sufficient to put theory into practice.

	The first part of the project, the emulator, was satisfactory. It is fit for the task at hand and can be extended further.

	Same can be said for the second part -- the communication interface.

	The third part, the machine learning program, is passable. It works with intermittent success, well enough to confirm the hypothesis. Previous knowledge would have been very helpful for the development of this part.

	Different components of the project work well together and are still autonomous enough to be used separately in another project.

	This research paper gives a cross-section of the chosen subjects. It is worth noting what holds true right now, might not in the future.

	During the work we learned both about specific areas and software development in general. What is more, we learned how to do the research necessary for such a project. 

	We would like to thank everybody who contributed, especially our supervisor Marika Anissimov.

	\begin{thebibliography}
		\bibitem{a} Emulator. Wiktionary. \url{https://en.wiktionary.org/wiki/emulator} (16.12.2018)
		\bibitem{b} Sockets. The GNU C Library. \url{https://www.gnu.org/savannah-checkouts/gnu/libc/manual/html_node/Sockets.html} (16.12.2018)
		\bibitem{c} About Processes and Threads. Microsoft Docs. \url{https://docs.microsoft.com/en-us/windows/desktop/ProcThread/about-processes-and-threads} (15.12.2018)
		\bibitem{d} ROM Definition. PC Magazine Encyclopedia. \url{https://www.pcmag.com/encyclopedia/term/50607/rom} (14.12.2018)
		\bibitem{e} Akhil Anand. What is memory mapped I/O? Quora. \url{https://www.quora.com/What-is-memory-mapped-I-O/answer/Akhil-Anand-34} (14.12.2018)
		\bibitem{f} Evitama. Eesti õigekeelsussõnaraamat ÕS 2018. \url{https://www.eki.ee/dict/qs/index.cgi?Q=evitama} (14.12.2018)
		\bibitem{g} Class. Python Software Foundation. \url{https://docs.python.org/3.6/tutorial/classes.html} (6.01.2019)
	\end{thebibliography}

	\appendices
	\appendix{Emulaatori klass}
	\begin{python}
	import time
	from debugger import Elmchey

	elmchey = Elmchey()

	elmchey.connect() # Emulaatoriga ühendamine
	elmchey.run() # Emulaatori käima panemine

	time.sleep(5)

	elmchey.pause() # Emulaatori pausile panemine

	screenshot = elmchey.screen_buffer() # Ekraanitõmmise saamine

	elmchey.disconnect() # Emulaatorist lahti ühendamine
	\end{python}

	\appendix{Sidekomponendi kasutamine masinõppeprogrammis}
	\begin{python}
	import numpy as np
	import time
	from PIL import Image
	import cv2

	from elmchey import Elmchey, config, GameError

	BYTEORDER, SIGNED = config.BYTEORDER, config.SIGNED

	class DrMario():

	    game = 'Dr. Mario'

	    def __init__(self):
	        self.debugger = Elmchey()
	        self.debugger.connect()
	        if not self.debugger.info['game'] == self.game:
	            self.debugger._fail()
	            raise GameError('Wrong game run!')
	        time.sleep(5) # Grey-screen otherwise.
	        self.debugger.run()
	        self._stepper = self.debugger.frame_stepper()
	        self.last_score = 0
	        self.init_game()

	    @property
	    def score(self):
	        """Get current game score."""
	        start, end = 0x984C, 0x9852
	        score = []
	        entry = self.debugger.read_memory(start, end - start)
	        for digit in entry:
	            if int(digit) in range(10):
	                score.append(str(digit))
	        return int(''.join(score + ['0']))

	    @property
	    def menu(self):
	        """Check whether current screen is menu."""
	        start, end =  0x9983, 0x9984
	        values = (0xFE, 0xC, 0x83, 0x93)
	        message = self.debugger.read_memory(start, end - start) or False
	        value = int.from_bytes(message, BYTEORDER, signed = SIGNED)
	        return value in values

	    @property
	    def terminal(self):
	        """Check whether game has terminated."""
	        return (self.menu == True) and (self.score == 0)

	    @property
	    def screen(self):
	        """Get current screen as image."""
	        image_bytes = self.debugger.screen_buffer()
	        pil_image = Image.frombytes('RGB', (160, 144), image_bytes)
	        cv2_image = np.array(pil_image)
	        cv2_image = cv2_image[:, :, ::-1].copy()
	        return cv2_image

	    def init_game(self):
	        """Initialize game."""
	        next(self._stepper)
	        while self.menu:
	            self.debugger.button('START', True)
	            next(self._stepper)

	    def frame_step(self, action):
	        """Take step and do actions."""

	        if self.terminal:
	            screen = self.screen
	            reward = self.score - self.last_score
	            terminal = True
	            self.init_game()
	        else:
	            while self.menu:
	                self.debugger.button('START', True)
	                next(self._stepper)

	            if action[1] == 1:
	                self.button('RIGHT', True)
	            elif action[2] == 1:
	                self.button('LEFT', True)
	            elif action[3] == 1:
	                self.button('UP', True)
	            elif action[4] == 1:
	                self.button('DOWN', True)
	            elif action[5] == 1:
	                self.button('A', True)
	            elif action[6] == 1:
	                self.button('B', True)

	            next(self._stepper)

	            screen = self.screen
	            reward = self.score - self.last_score
	            terminal = False

	        return screen, reward, terminal
	\end{python}
\end{document}
